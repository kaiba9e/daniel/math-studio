#!/usr/bin/python3

"""
There is a sequence {an}, where a1 = 8 and a2 = 9. Starting from the third term, each number
written down is the units digit of the product of its two preceding terms. For example, according
to the rule, a3 = 2. Find a2023. 
"""

import sys


def main(argc, argv):
    if argc != 2:
        print("Usage: %s <N>" % argv[0], file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s 2023" % argv[0], file=sys.stderr)

    a = [-1, 8, 9, 2] # a[0], a[1]=8, a[2]=9, a[3]=2
    i = len(a)        # next one should be a[4]
    while i <= int(argv[1]):
        a_next = (a[-1] * a[-2]) % 10
        a.append(a_next)
        i += 1
    print(a)
    print("a%d = %d" % (len(a) - 1, a[-1]))
    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))

"""
TMC01$ /tmp/foo.py 4
[-1, 8, 9, 2, 8]
a4 = 8
TMC01$ /tmp/foo.py 5
[-1, 8, 9, 2, 8, 6]
a5 = 6
TMC01$ /tmp/foo.py 6
[-1, 8, 9, 2, 8, 6, 8]
a6 = 8
TMC01$ /tmp/foo.py 7
[-1, 8, 9, 2, 8, 6, 8, 8]
a7 = 8
TMC01$ /tmp/foo.py 8
[-1, 8, 9, 2, 8, 6, 8, 8, 4]
a8 = 4
TMC01$ /tmp/foo.py 9
[-1, 8, 9, 2, 8, 6, 8, 8, 4, 2]
a9 = 2
TMC01$ /tmp/foo.py 10
[-1, 8, 9, 2, 8, 6, 8, 8, 4, 2, 8]
a10 = 8
TMC01$ /tmp/foo.py 11
[-1, 8, 9, 2, 8, 6, 8, 8, 4, 2, 8, 6]
a11 = 6
TMC01$ /tmp/foo.py 12
[-1, 8, 9, 2, 8, 6, 8, 8, 4, 2, 8, 6, 8]
a12 = 8
TMC01$ /tmp/foo.py 13
[-1, 8, 9, 2, 8, 6, 8, 8, 4, 2, 8, 6, 8, 8]
a13 = 8
TMC01$ /tmp/foo.py 2023
...
a2023 = 8
"""
