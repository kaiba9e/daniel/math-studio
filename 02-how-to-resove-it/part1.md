# PART I. IN THE CLASSROOM

## PURPOSE


**`1. Helping the student.`** One of the most important tasks of the teacher is to help his students. This task is not quite easy; it demands time, practice, devotion, and sound principles.

The student should acquire as much experience of independent work as possible. But if he is left alone with his problem without any help or with insufficient help, he may make no progress at all. If the teacher helps too much, nothing is left to the student. The teacher should help, but not too much and not too little, so that the student shall have _a reasonable share of the work_.

If the student is not able to do much, the teacher should leave him at least some illusion of independent work. In order to do so, the teacher should help the student discreetly, _unobtrusively_.

The best is, however, to help the student naturally.  The teacher should put himself in the student's place, he should see the student's case, he should try to understand what is going on in the student's mind, and ask a question or indicate a step that _could have occurred to the student himself_.


**`2. Questions, recommendations, mental operations.`** Trying to help the student effectively but unobtrusively and naturally, the teacher is led to ask the same questions and to indicate the same steps again and again. Thus, in countless problems, we have to ask the question: What is the unknown? We may vary the words, and ask the same thing in many different ways: What is required?  What do you want to find? What are you supposed to seek? The aim of these questions is to focus the student's attention upon the unknown. Sometimes, we obtain the same effect more naturally with a suggestion: Look at the unknown! Question and suggestion aim at the same effect; they tend to provoke the same mental operation.

It seemed to the author that it might be worth while to collect and to group questions and suggestions which are typically helpful in discussing problems with students. The list we study contains questions and suggestions of this sort, carefully chosen and arranged; they are equally useful to the problem-solver who works by himself. If the reader is sufficiently acquainted with the list and can see, behind the suggestion, the action suggested, he may realize that the list enumerates, indirectly, mental operations typically useful for the solution of problems. These operations are listed in the order in which they are most likely to occur.


**`3. Generality`** is an important characteristic of the questions and suggestions contained in our list. T ake the questions: What is the unknown? What are the data?  What is the condition? These questions are generally applicable, we can ask them with good effect dealing with all sorts of problems. Their use is not restricted to any subject-matter. Our problem may be algebraic or geometric, mathematical or nonmathematical, theoretical or practical, a serious problem or a mere puzzle; it makes no difference, the questions make sense and might help us to solve the problem.

There is a restriction, in fact, but it has nothing to do with the subject-matter. Certain questions and suggestions of the list are applicable to "problems to find" only, not to "problems to prove." If we have a problem of the latter kind we must use different questions; see PROBLEMS TO FIND, PROBLEMS TO PROVE.


**`4. Common sense.`** The questions and suggestions of our list are general, but, except for their generality, they are natural, simple, obvious, and proceed from plain common sense. Take the suggestion: Look at the unknown!  And try to think of a familiar problem having the same or a similar unknown. This suggestion advises you to do what you would do anyhow, without any advice, if you were seriously concerned with your problem.  Are you hungry? You wish to obtain food and you think of familiar ways of obtaining food. Have you a problem of geometric construction? You wish to construct a triangle and you think of familiar ways of constructing a triangle. Have you a problem of any kind?  You wish to find a certain unknown, and you think of familiar ways of finding such an unknown, or some similar unknown. If you do so you follow exactly the suggestion we quoted from our list. And you are on the right track, too; the suggestion is a good one, it suggests to you a procedure which is very frequently successful.

All the questions and suggestions of our list are natural, simple, obvious, just plain common sense; but they state plain common sense in general terms. They suggest a certain conduct which comes naturally to any person who is seriously concerned with his problem and has some common sense. But the person who behaves the right way usually does not care to express his behavior in clear words and, possibly, he cannot express it so; our list tries to express it so.


**`5. Teacher and student. Imitation and practice.`** There are two aims which the teacher may have in view when addressing to his students a question or a suggestion of the list: First, to help the student to solve the problem at hand. Second, to develop the student's ability so that he may solve future problems by himself.

Experience shows that the questions and suggestions of our list, appropriately used, very frequently help the student. They have two common characteristics, common sense and generality; As they proceed from plain common sense they very often come naturally; they could have occurred to the student himself. As they are general, they help unobtrusively; they just indicate a general direction and leave plenty for the student to do.

But the two aims we mentioned before are closely connected; if the student succeeds in solving the problem at hand, he adds a little to his ability to solve problems.  Then, we should not forget that our questions are general, applicable in many cases. If the same question is repeatedly helpful, the student will scarcely fail to notice it and he will be induced to ask the question by himself in a similar situation. Asking the question repeatedly, he may succeed once in eliciting the right idea. By such a success, he discovers the right way of using the question, and then he has really assimilated it.

The student may absorb a few questions of our list so well that he is finally able to put to himself the right question in the right moment and to perform the corresponding mental operation naturally and vigorously.  Such a student has certainly derived the greatest possible profit from our list. What can the teacher do in order to obtain this best possible result?

Solving problems is a practical skill like, let us say, swimming. We acquire any practical skill by imitation and practice. Trying to swim, you imitate what other people do with their hands and feet to keep their heads above water, and, finally, you learn to swim by practicing swimming. Trying to solve problems, you have to observe and to imitate what other people do when solving problems and, finally, you learn to do problems by doing them.

The teacher who wishes to develop his students' ability to do problems must instill some interest for problems into their minds and give them plenty of opportunity for imitation and practice. If the teacher wishes to develop in his students the mental operations which correspond to the questions and suggestions of our list, he puts these questions and suggestions to the students as often as he can do so naturally. Moreover, when the teacher solves a problem before the class, he should dramatize his ideas a little and he should put to himself the same questions which he uses when helping the students. Thanks to such guidance, the student will eventually discover the right use of these questions and suggestions, and doing so he will acquire something that is more important than the knowledge of any particular mathematical fact.
